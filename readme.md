# Gitflow with maven SCM

The project consists of a HelloWorld java application for which we use Gitflow as branching strategy.

The following branches are permanent:
* **master** - branch that resembles the code that runs now in production
* **develop** - the development integration branch

There are few temporary branches also:
* **feature-#** - supporting branch to develop a feature and later to integrate back to develop
* **hot-fix-#** - hot-fix branch that branches of from **master** and later integrate into all active branches
* **release** - release branch containing the work from the previous sprint to be tested and bug-fixed during acceptance testing phase.

The constraint is to have only one **release** branch at a time.

## 1. Prepare the pom.xml

It needs **<scm>** entries, **<distributionManagement>** entries (to know where to deploy the release artifact) and few options for the maven-release-plugin (org.apache.maven.plugins maven-release-plugin 2.3.2 v@{project.version}) false true

Few explanation here :
* **tagNameFormat** is here to change the default tag name (which is ${project.artifactId}-${project.version}) to a better one.
* **pushChanges** set to false tells maven-release-plugin not to push changes (this will become useful)
* **localCheckout** set to true tells maven-release-plugin to clone from local repository (not distant). This is especially useful here because we didn’t push anything (so not setting this option would result in a failure).

## 2. Development process

### 2.1. Develop feature and integrate on **develop** integration branch

### 2.2. Sprint end: create a release branch from develop

* **$ mvn release:branch -B -DbranchName=release -DautoVersionSubmodules=true** - create release branch and bump the version

### 2.3. Continue on **develop** the development for the current sprint

* **$ git checkout develop** - get back to **develop**
* Develop features for the current sprint

### 2.4. Fix bugs on **release** branch

* **$ git checkout release** - get back to **release**
* fix bugs during release testing phase (UAT)

### 2.5 Release ready to be released in production

* **$ git checkout release** - get back to **release**
* **$ mvn release:prepare -B** - prepare the release
* **$ mvn release:perform -Darguments="-Dmaven.deploy.skip=true"** - build final version and deploy

### 2.6. Merge **release** in **master** and rest

* **$ git checkout master** - switch to **master**
* **$ git merge --no-ff release~1 -m "[RELEASE] merge release into master"** - merge into **master** without the last commit
* merge in all **hot-fix** branches
* **$ git checkout develop** - switch to **develop**
* **$ git merge --no-ff release -m "[RELEASE] merge release into develop"** - merge into **develop** without the last commit
* **$ git branch -D release** - delete **release** branch
* **$ git push -u origin :release** - delete **release** branch from origin
* **$ git push --all && git push --tags** - finally push everything
