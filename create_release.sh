#!/bin/bash
# OBSOLETE
#
# Bump version, build and deploy
echo "Switch to the develop branch"
git checkout -b release develop

echo "Change the pom, commit and tag version, and re-change pom (by incrementing SNAPSHOT version)"
mvn -B release:prepare

echo "get the tagged version, compile and don't deploy"
mvn release:perform -Darguments="-Dmaven.deploy.skip=true"

echo "Get back to the **develop** branch"
git checkout develop

echo "merge the version back into **develop**"
git merge --no-ff -m "[create release] merge relese into develop" release

echo "merge the version back into **develop**"
git checkout release

echo "set upstream tracking"
git branch --set-upstream-to=origin/release release

echo "restore the **release** the stable version in pom.xml by reverting the last commit"
git reset --hard HEAD~1

echo "force push the **release** branch"
git push -f
git push --all

echo "Change to **develop**"
git checkout develop
