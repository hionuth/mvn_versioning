#!/bin/bash

# This script automates the release steps.
#
# Before lunching the script it is recommended to merge the release branch into
#   develop in order to integrate the release fixes into the current sprint
#   work.

git checkout release
mvn release:prepare -B
mvn release:perform
git checkout master
git merge --no-ff release~1 -m "[RELEASE] merge release into master"

# Here you should merge master in all "active" hot-fix branches !!!

git checkout develop
git merge --no-ff release -m "[RELEASE] merge release into develop"
git branch -D release
git push -u origin :release
git push --all && git push --tags 
